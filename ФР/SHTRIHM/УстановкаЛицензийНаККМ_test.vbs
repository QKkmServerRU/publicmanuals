  Dim Driver 
  Set Driver = CreateObject("Addin.DRvFR")	' подключаем драйвер
  Driver.ConnectionType = 6			' тип подключения TCP сокет (тут надо уточнить как у вас подключаются ККТ)
  Driver.ProtocolType = 0			'    
  Driver.UseIPAddress = True			'
  Driver.IPAddress = "192.168.137.111"		' у меня ККТ подключена через RNDIS поэтому я цепляюсь по IP
  Driver.TCPPort = 7778				'
  Driver.Timeout = 1000	
  Driver.Password = 30				' пароль оператора
  Driver.Connect()
  
  Driver.ReadSerialNumber ' читаем заводской номер, который потом получим из параметра SerialNumber
  ZN = Driver.SerialNumber 

  if Not ZN = "" then ' если получили заводской номер, значит подключились к кассе и можем продолжать	

  	Set fso = CreateObject("Scripting.FileSystemObject")
  	Set f1 = fso.OpenTextFile("D:\testshtrih\11_01012019_31122020_1076.slf", 1) 'путь к файлу со списком ККТ и лицензий на них

  	Do Until f1.AtEndOfStream 			' пройдемся по всему файлу со списком лицензий в поисках лицензии дл нашей ККТ
 		s = f1.ReadLine				' в строке три столбца 1 - номер ККТ, 2 - лицензия в HEX представлении, 3 - цифровая подпись лицензии в HEX представлении
  		if Left(s,16) = ZN then			' если нашли данные для нашей ККТ
		  Driver.License = Mid(s,18,128)	' укажем лицензию 
		  Driver.DigitalSign = Right(s,128)	' укажем цифровую подпись
		  Driver.WriteFeatureLicenses		' запишем лицензию в ККТ
 		 end if
	loop

  	Driver.Disconnect
  end if